﻿namespace MnistClassificationTests

//open MnistClassification
//open TestRun
//open Xunit
//open FsUnit.Xunit
//
//type ``Given the MNIST classification set`` ()=
//    let testError = computeResults rand props trainingSet testSet 10
//
//    [<Fact>] member test.
//        ``The test error should be less than 10%.``()=
//        testError |> should lessThanOrEqualTo 0.1
